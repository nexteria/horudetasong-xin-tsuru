# Windows x64向けファイル送信ツール

## ファイル構成
- winscp
 ファイル転送用のライブラリです
- ファイル送信ツール.bat
 ファイル送信ツールの実行ファイルです。これを起動してください。
- ファイル送信ツール.ps1
 ファイル送信ツールの実態です。
 
## 設定
「ファイル送信ツール.ps1」を開いて以下の箇所を修正してください。

32行目： HostName = "[ホスト名を入力してください]"  
33行目： UserName = "[ユーザ名を入力してください]"  
34行目： Password = "[パスワードを入力してください]"  
35行目： PortNumber = "[ポート番号を入力してください]"  
52行目： $transferResult = $session.PutFiles($_.FullName, "/[店舗フォルダ名]", $False, $transferOptions)

## トラブルシューティング
### スクリプトの実行が無効になっている
以下のようなエラーが出ます
```text
このシステムではスクリプトの実行が無効になっているため、ファイル XXXXXXXXX.ps1 を読み込むことができませ
ん。詳細については、「about_Execution_Policies」(https://go.microsoft.com/fwlink/?LinkID=135170) を参照してください。
```

### 原因
初期設定では Restricted の実行ポリシーとなっています。  
このポリシーではすべてのスクリプトの実行が制限されています。  

### 対処方法
以下のコマンドをコマンドプロンプトから実行します。
```bat
PowerShell Set-ExecutionPolicy RemoteSigned
```