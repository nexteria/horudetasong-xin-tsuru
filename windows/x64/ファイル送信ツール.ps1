. ".\\ライブラリ.ps1"
try
{
	$filelist = $Null;
	$senddir = $Null;

	# ディレクトリの検索
	If($Args.Length -eq 0)
	{
		# 引数が指定されていないときはカレントを検索する
		$filelist = (Get-ChildItem | Where-Object { `
			($_.Name -ne 'winscp') `
			-and ($_.Name -ne 'ファイル送信ツール.ps1') `
			-and ($_.Name -ne 'ファイル送信ツール.bat') `
			-and ($_.Name -ne '設定.ini') `
			-and ($_.Name -ne 'README.md') `
			-and ($_.Name -ne 'ライブラリ.ps1')
		})
	}
	ElseIf(($Args.Length -eq 1) -or ($Args.Length -eq 2))
	{
		# ファイルの存在チェック
		If(Test-Path $Args[0])
		{
			$filelist = (Get-ChildItem $Args[0])
		}else{
			throw "ファイルが存在しません"	
		}
		
		if($Args.Length -eq 2)
		{
			$senddir = $Args[1]
		}
	}
	Else
	{
		throw "引数異常"
	}

    # Load WinSCP .NET assembly
    Add-Type -Path '.\winscp\WinSCPnet.dll'

    # Setup session options
    $sessionOptions = New-Object WinSCP.SessionOptions -Property @{
        Protocol = [WinSCP.Protocol]::Sftp
        HostName = $($global:setting['ホスト名'])
        UserName = $($global:setting['ユーザ名'])
        Password = $($global:setting['パスワード'])
        PortNumber = $($global:setting['ポート番号'])
        GiveUpSecurityAndAcceptAnySshHostKey = $True
    }

    $session = New-Object WinSCP.Session

    try
    {
        # Connect
        $session.Open($sessionOptions)

        # Force binary mode transfer
        $transferOptions = New-Object WinSCP.TransferOptions
        $transferOptions.TransferMode = [WinSCP.TransferMode]::Binary

        # ファイル送信
		$filelist | ForEach-Object{

			# 送信先フォルダ名がある場合
			if($senddir -ne $Null)
			{
				$transferResult = $session.PutFiles($_.FullName, "/$($senddir)/", $False, $transferOptions)
			}else
			{
				$transferResult = $session.PutFiles($_.FullName, "/$($global:setting['通常店舗フォルダ名'])/", $False, $transferOptions)
			}

			# Throw on any error to emulate the default "option batch abort"
			$transferResult.Check()
		}
    }
    finally
    {
        # Disconnect, clean up
        $session.Dispose()
    }

    exit 0
}
catch
{
    $errorString= $_ | Out-String 
    Write-Host $errorString
    exit 1
}
