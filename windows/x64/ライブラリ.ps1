$global:setting = @{}

$lines = get-content ".\\設定.ini"
foreach($line in $lines){
  # コメントと空行を除外する
  if($line -match "^$"){ continue }
  if($line -match "^\s*;"){ continue }

  $param = $line.split("=",2)

  $global:setting.add($param[0].trim(), $param[1].trim())
}